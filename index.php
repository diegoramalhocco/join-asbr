<?php 
date_default_timezone_set('America/Sao_Paulo');
require_once 'include/constantes.inc.php';
require_once 'autoload.php';

use ActualSales\classe\View;
use ActualSales\classe\Controller\ControllerIndex;

new View();
new ControllerIndex();

?>