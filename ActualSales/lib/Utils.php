<?php

namespace ActualSales\lib;
/**
 * Classe de apoio
 * @author diego.silva
 *
 */
class Utils{
	
	/**
	 * Recebe uma data no formato 'dd/mm/aaaa hh:mm:ss'
	 *e retona no formato aaaa-mm-dd
	 */
	static function formatDate($date){
			//Tira a hora da data
			$temp = explode(' ', $date);
			$date = $temp[0];
		
			return $date = implode('-', array_reverse(explode('/', $date)));
		
		return $date = implode('-', array_reverse(explode('/', $date)));
		
	}
	
	/**
	 * Recebe uma data no formato 'dd/mm/aaaa hh:mm:ss'
	 *e retona no formato aaaa-mm-dd
	 */
	static function formatDateString($data){
		if($data == "" || $data == "null"){
			return null;
		}
		$data_partes = explode(" ", $data);
		$data_nova = implode(preg_match("~\/~", $data_partes[0]) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data_partes[0]) == 0 ? "-" : "/", $data_partes[0])));
		return $data_nova." ".$data_partes[1];
	}
	
	static function calculaIdade($data, $dataAtual){
		return \DateTime::createFromFormat('Y-m-d', $data)->diff(new \DateTime($dataAtual))->y;
	}
	
	/**
	 * Envia transações GET/POST
	 * @param $url
	 * @param $params (Campos do form)
	 * @return boolean/result
	 */
	static function enviarRequisicaoGetPostHttp($url, $params){
		$string = '';
		
		foreach($params as $key=>$value) {
			$string .= $key.'='.$value.'&'; 
		}
		
		rtrim($string, '&');
		
		$ch = curl_init();
		
		curl_setopt($ch,CURLOPT_URL, URL_ENDPOINT);
		curl_setopt($ch,CURLOPT_POST, count($params));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $string);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_exec($ch);
		$rs = curl_multi_getcontent ( $ch );
		
		curl_close($ch);
		
		return json_decode($rs);
	}
	
	/**
	 * Remove traços e pontos de uma string
	 */
	static function clearString($string){
		$pontos = array("-", " ", "(", ")");
		return str_replace($pontos, "", $string);
	}
}
?>