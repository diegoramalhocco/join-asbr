<?php
namespace ActualSales\classe\Dao;

use ActualSales\classe\Conexao;
use ActualSales\classe\Model\ModelCliente;

/**
 * Classe responsável pelas persistêcias no DB
 * da tabela tbl_cliente
 * @author DIEGO
 */
class Daocliente extends Conexao{
	
	protected $name = 'tbl_cliente';
	 
	/**
	 * Método que envia os dados para o DB
	 */
	public function save(ModelCliente $model){

		$conn = $this->conectar();
		$stmt = $conn->prepare("
				INSERT INTO $this->name (
				nome,
				email,
				telefone,
				dataNascimento
				)
				VALUES(?,?,?,?)"
				);
		
		$stmt->bind_param('ssss',
				$model->getNome(),
				$model->getEmail(),
				$model->getTelefone(),
				$model->getDataNascimento()
			);
		$stmt->execute();
		
		$model->setIdCliente($stmt->insert_id);
		
	}
}
?>
