<?php
namespace ActualSales\classe\Dao;

use ActualSales\classe\Conexao;
use ActualSales\classe\Model\ModelCliente;

/**
 * Classe responsável pelas persistêcias no DB
 * da tabela tbl_cliente
 * @author DIEGO
 */
class DaoRegiaoUnidade extends Conexao{
	
	/**
	 * Método que retorna os id's de Regiao e Unidade
	 * que serão usados na tabela de tbl_regiao_unidade
	 */
	public function findRegiaoUnidade(ModelCliente $model){
		//retorna o idRegiao
		$idRegiao = $this->findRegiao($model->getRegiao());
		
		//retorna o idUnidade
		$idUnidade = $this->findUnidade($model->getUnidade());
		
		$sql = "SELECT RU.* FROM tbl_regiao_unidade AS RU
		JOIN tbl_regiao AS R ON R.idRegiao = RU.idRegiao
		JOIN tbl_unidade AS U ON U.idUnidade = RU.idUnidade
		WHERE U.idUnidade = ? AND R.idRegiao = ?";
		
		$rs = $this->get_result($sql, 'dd', array($idUnidade, $idRegiao));
		
		return $rs[0]['idRegiaoUnidade'];
	}
	
	/**
	 * Método que retorna o id da Regiao
	 * @param string $str
	 * @return mixed
	 */
	public function findRegiao($str){
		$sql = "SELECT idRegiao FROM tbl_regiao WHERE regiao like ?";
		
		$rs = $this->get_result($sql, 's', array($str));
		
		return $rs[0]['idRegiao']; 
	}
	
	/**
	 * Método que retorna o id da Unidade
	 * @param string $str
	 * @return mixed
	 */
	public function findUnidade($str){
		$sql = "SELECT idUnidade FROM tbl_unidade WHERE unidade like ?";
	
		$rs = $this->get_result($sql, 's', array($str));
		
		return $rs[0]['idUnidade'];
	}
}
?>
