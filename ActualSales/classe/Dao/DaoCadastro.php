<?php
namespace ActualSales\classe\Dao;

use ActualSales\classe\Conexao;
use ActualSales\classe\Model\ModelCadastro;

/**
 * Classe responsável pelas persistêcias no DB
 * da tabela tbl_cadastro
 * @author DIEGO
 */
class DaoCadastro extends Conexao{
	
	protected $name = 'tbl_cadastro';
	 
	/**
	 * Método que envia os dados para o DB
	 */
	public function save(ModelCadastro $model){
		
		$conn = $this->conectar();
		$stmt = $conn->prepare("
				INSERT INTO $this->name (
				idCliente,
				idRegiaoUnidade,
				pontuacao
				)
				VALUES(?,?,?)"
				);
		
		$stmt->bind_param('sss',
				$model->getIdCliente(),
				$model->getIdRegiaoUnidade(),
				$model->getPontuacao()
				);
		$stmt->execute();
		
		$model->setIdCadastro($stmt->insert_id);
		
	}
}
?>
