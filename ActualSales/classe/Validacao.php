<?php

namespace ActualSales\classe;

/**
 * Classe responsável pela Validacao dos Elementos
 * @since	1.0
 */
class Validacao{
	
	private $status = true;
	public $message = 'Campos Inválidos: ';
	
	public function __construct($data){
		
		$this->validateNome($data['nome']);
		$this->validateDataNascimento($data['data_nascimento']);
		$this->validateTelefone($data['telefone']);
		$this->validateEmail($data['email']);
	}
	
	/**
	 * Valida o Campo Nome 
	 * @param String $value
	 */
	public function validateNome($value){
		
		if(empty($value)){
			$this->status = false;
			$this->message .= "Nome, ";
			return;
		}
		
		$exp = "/^([a-záâãéêíóôõúç]+[ ]{1,1}[a-záâãéêíóôõúç]+)([a-záâãéêíóôõúç ]{0,})$/i";
		
		if (!preg_match($exp, $value)){
			$this->status = false;
			$this->message .= "Nome, ";
		}
		
	}
	
	/**
	 * Valida o Campo DataNascimento
	 * @param String $value
	 */
	public function validateDataNascimento($value){
		
		if(empty($value)){
			$this->status = false;
			$this->message .= "Data de Nascimento, ";
			return;
		}
		
		$rs = explode('-', $value);
		
		if(! checkdate((int)$rs[1], (int)$rs[2], (int)$rs[0])){
			$this->status = false;
			$this->message .= "Data de Nascimento, ";
		}
	}
	
	/**
	 * Valida o Campo Email
	 * @param String $value
	 */
	public function validateEmail($value){
		
		if(empty($value)){
			$this->status = false;
			$this->message .= "Email,";
			return;
		}
		
		$emailTemp = array();
		
		//Expressao Regular
		preg_match_all('/^(([a-z0-9_]|\-|\.)+@(([a-z0-9_]|\-)+\.)+[a-z]{2,4})$/i', $value, $emailTemp);
		
		if(!$emailTemp[0][0]){
			$this->status = false;
			$this->message .= "Email,";
		};
	}
	
	/**
	 * Valida o Campo Telefone
	 * @param String $value
	 */
	public function validateTelefone($value){
		
		if(empty($value)){
			$this->status = false;
			$this->message .= "Telefone, ";
			return;
		}
		
		$exp = "/^[0-9]{10,11}+$/i";
		
		if (!preg_match($exp, $value)){
			$this->status = false;
			$this->message .= "Telefone, ";
		}
		
	}
	
	public function isValid(){
		
		return $this->status;
	}
}
?>

