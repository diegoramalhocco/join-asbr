<?php

namespace ActualSales\classe;

/**
 * Classe responsável pela conexão com o banco
 * @since	1.0
 */

class Conexao{

	private $host=DATABASE_HOST;
	private $db = DATABASE_NAME;
	private $user=DATABASE_USER;
	private $pass=DATABASE_PASSWORD;
	private $port=DATABASE_PORT;

	/**
	 * Cria a conexão mysqli
	 * @return mysqli
	 */
	
	protected function conectar(){
		$conn = new \mysqli($this->host, $this->user, $this->pass,$this->db) or die("erro");
		$conn->set_charset("utf8");

		return $conn;
	}
	
	/**
	 * Método responsável por montar a query com o preparate statment
	 * e retorar um array com os dados
	 * @since	1.0
	 * @param	string $sql
	 * @param	string $types
	 * @param	array $params
	 * @return	array $result
	 */
	protected function get_result($sql,$types = null,$params = null){
	
		$conn = $this->conectar();
		$result = array();
		$c = 0;
		
		//cria o prepare
		$stmt = $conn->prepare($sql);
		
		if($types&&$params){
			$bind_names[] = $types;
	
			for ($i=0; $i<count($params);$i++){
				$bind_name = 'bind' . $i;
				$$bind_name = $params[$i];
				$bind_names[] = &$$bind_name;
			}
	
			$return = call_user_func_array(array($stmt,'bind_param'),$bind_names);
		}
		
		$stmt->execute();
	
		$meta = $stmt->result_metadata();
	
		while ($field = $meta->fetch_field()) {
			$var = $field->name;
			$$var = null;
			$parameters[$field->name] = &$$var;
		}
	
		call_user_func_array(array($stmt, 'bind_result'), $parameters);
	
		while($stmt->fetch()){
			foreach($parameters as $k=>$v){
				$result[$c][$k] = $v;
			}
			$c++;
		}
	
		$stmt->close();
	
		return $result;
	}
	
}

?>