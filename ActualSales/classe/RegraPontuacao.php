<?php

namespace ActualSales\classe;

use ActualSales\lib\Utils;

/**
 * Classe responsável pelas Regras de pontuação dos leads
 * @since	1.0
 */
class RegraPontuacao extends Pontuacao{
	
	public $data;
	
	function __construct($data){
		$this->data = $data;
		
		$this->setPontuacaoInicial(10);
		$this->calculoRegiao();
		$this->calculoIdade();
		
		return $this->getPontuacaoFinal();
		
	}
	
	function calculoRegiao(){
		switch ($this->data['regiao']) {
			case 'Sul':
				$this->removePontuacao(2);
				break;
			case 'Sudeste':
				if($this->data['unidade'] != 'Sao Paulo'){
					$this->removePontuacao(1);
				}
				break;
			case 'Centro-Oeste':
				$this->removePontuacao(3);
				break;
			case 'Nordeste':
				$this->removePontuacao(4);
				break;
			case 'Norte':
				$this->removePontuacao(5);
				break;
		}
	}

	/*
	 * Calcula a pontuacao da idade com base nos seguintes critérios
	 - A partir de 100 ou menor que 18: -5 pontos
	 - Entre 40 e 99: -3 pontos
	 - Entre 18 e 39: não modifica
	 */
	function calculoIdade(){
		
		$idade = Utils::calculaIdade($this->data['data_nascimento'], '2016-06-01');
		
		switch ($idade) {
			case ($idade >= 100):
			case ($idade < 18 ):
				$this->removePontuacao(5);
				break;
			case ($idade >= 40 && $idade <= 99):
				$this->removePontuacao(3);
				break;
			case ($idade >= 18 && $idade <= 39):
				//nao remove pontuacao
				break;
		}
	}
}
?>


