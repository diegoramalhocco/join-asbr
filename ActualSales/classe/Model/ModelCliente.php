<?php
namespace ActualSales\classe\Model;

/**
 * Model de Cliente
 * @author DIEGO
 */
class ModelCliente {
	
	private $idCliente;
	private $nome;
	private $email;
	private $telefone;
	private $dataNascimento;
	private $regiao;
	private $unidade;
	
	public function getNome() {
		return $this->nome;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}
	public function getTelefone() {
		return $this->telefone;
	}
	public function setTelefone($telefone) {
		$this->telefone = $telefone;
		return $this;
	}
	public function getDataNascimento() {
		return $this->dataNascimento;
	}
	public function setDataNascimento($dataNascimento) {
		$this->dataNascimento = $dataNascimento;
		return $this;
	}
	public function getIdCliente() {
		return $this->idCliente;
	}
	public function setIdCliente($idCliente) {
		$this->idCliente = $idCliente;
		return $this;
	}
	public function getRegiao() {
		return $this->regiao;
	}
	public function setRegiao($regiao) {
		$this->regiao = $regiao;
		return $this;
	}
	public function getUnidade() {
		return $this->unidade;
	}
	public function setUnidade($unidade) {
		$this->unidade = $unidade;
		return $this;
	}
	
	
	
	
}
?>
