<?php
namespace ActualSales\classe\Model;

/**
 * Model de Cadastro
 * @author DIEGO
 */
class ModelCadastro {
	
	
	private $idCadastro;
	private $idCliente;
	private $idRegiaoUnidade;
	private $pontuacao;
	
	public function getIdCadastro() {
		return $this->idCadastro;
	}
	public function setIdCadastro($idCadastro) {
		$this->idCadastro = $idCadastro;
		return $this;
	}
	public function getIdCliente() {
		return $this->idCliente;
	}
	public function setIdCliente($idCliente) {
		$this->idCliente = $idCliente;
		return $this;
	}
	public function getIdRegiaoUnidade() {
		return $this->idRegiaoUnidade;
	}
	public function setIdRegiaoUnidade($idRegiaoUnidade) {
		$this->idRegiaoUnidade = $idRegiaoUnidade;
		return $this;
	}
	public function getPontuacao() {
		return $this->pontuacao;
	}
	public function setPontuacao($pontuacao) {
		$this->pontuacao = $pontuacao;
		return $this;
	}
	
}
?>
