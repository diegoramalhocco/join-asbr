<?php

namespace ActualSales\classe;

/**
 * Classe responsável pela Pontuacao dos leads
 * @since	1.0
 */
class Pontuacao{
	
	private $pontuacaoInicial;
	private $pontuacaoFinal;
	private $pontuacao;
	
	public function getPontuacaoInicial() {
		return $this->pontuacaoInicial;
	}
	
	public function setPontuacaoInicial($pontuacaoInicial) {
		$this->pontuacao = $pontuacaoInicial;
		return $this;
	}
	
	public function getPontuacaoFinal() {
		return $this->pontuacao;
	}
	
	public function addPontuacao($valor){
		$this->pontuacao += $valor;
		
	}
	
	public function removePontuacao($valor){
		$this->pontuacao -= $valor;
	}
	
	public function limparPontuacao(){
		$this->setPontuacao(0);
	}
	
	private function getPontuacao() {
		return $this->pontuacao;
	}
	private function setPontuacao($pontuacao) {
		$this->pontuacao = $pontuacao;
		return $this;
	}
}
?>
