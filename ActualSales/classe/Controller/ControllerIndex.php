<?php
namespace ActualSales\classe\Controller;

use ActualSales\classe\View;
use ActualSales\classe\Validacao;
use ActualSales\classe\RegraPontuacao;
use ActualSales\classe\Dao\DaoCadastro;
use ActualSales\classe\Model\ModelCliente;
use ActualSales\lib\Utils;
use ActualSales\classe\Model\ModelCadastro;
use ActualSales\classe\Dao\Daocliente;
use ActualSales\classe\Dao\DaoRegiaoUnidade;

/**
 * Controller responsável pela gerenciamento das 
 * informações que serão enviados para o DB e
 * pera o leads
 * @author DIEGO
 */
class ControllerIndex {
	protected $model;
	protected $data;
	protected $messages = array();

	function __construct($st_view = null, $v_params = null){
		$this->headerView();
		$this->contentView();
		$this->footerView();
	}
	
	/**
	 * Renderiza o cabeçalho da página
	 */
	function headerView(){
		$view = new View('classe/View/header.phtml');
		$view->setParams(array(
				'p1' => '1'
		));
		echo $view->showContents();
	}
	
	/**
	 * Renderiza o conteúdo da página
	 */
	function contentView(){
		
		if($_POST){
			$this->data = $_POST;
			
			$this->data['data_nascimento'] = Utils::formatDate($_POST['data_nascimento']);
			$this->data['telefone'] = Utils::clearString($_POST['telefone']);
			
			$validacao = new Validacao($this->data);
			
			if($validacao->isValid()){
				
				//Calcula a Pontuação
				$pontuacao = new RegraPontuacao($this->data);
				$this->data['score'] = $pontuacao->getPontuacaoFinal();
				
				//Envia os dados para o DB
				$this->saveDB();
				//Envia os dados leads 
				$this->enviarLeads();
				
				$this->messages['post'] = array('success' => true,'message' => 'ok');
			}else{
				$this->messages['post'] = array('success' => $validacao->isValid(),'message' => $validacao->message);
			}
			
		}
		
		$view = new View('classe/View/content.phtml');
		$view->setParams(array(
			'p1' => '1',
			'messages' => $this->messages
		));
		echo $view->showContents();
	}
	
	/**
	 * Renderiza o Rodapé da Página
	 */
	function footerView(){
		
		$view = new View('classe/View/footer.phtml');
		$view->setParams(array(
		));
	
		echo $view->showContents();
	}
	
	/**
	 * Envio dos dados para o DB
	 */
	private function saveDB(){
		
		$modelCliente = new ModelCliente();
		$modelCliente->setNome($this->data['nome']);
		$modelCliente->setEmail($this->data['email']);
		$modelCliente->setDataNascimento(Utils::formatDate($this->data['data_nascimento']));
		$modelCliente->setTelefone($this->data['telefone']);
		$modelCliente->setRegiao($this->data['regiao']);
		$modelCliente->setUnidade($this->data['unidade']);
		
		/*
		 * Insere o Cliente no Banco 
		 */
		$daoCliente = new Daocliente();
		$daoCliente->save($modelCliente);
		
		/*
		 * Insere o Cadastro no Banco
		 */
		$daoRegiao = new DaoRegiaoUnidade();
		$idRegiaoUnidade = $daoRegiao->findRegiaoUnidade($modelCliente);
		
		$modelCadastro = new ModelCadastro();
		$modelCadastro->setIdCliente($modelCliente->getIdCliente());
		$modelCadastro->setIdRegiaoUnidade($idRegiaoUnidade);
		$modelCadastro->setPontuacao($this->data['score']);
		
		$daoCadastro = new DaoCadastro();
		$daoCadastro->save($modelCadastro);
		
	}
	
	/**
	 * Enviar os dados para http://api.actualsales.com.br/join-asbr/ti/lead
	 */
	private function enviarLeads(){
		$params = $this->data;
		$params = array(
				'nome' => urlencode($this->data['nome']),
				'email' => $this->data['email'],
				'telefone' => urlencode($this->data['telefone']),
				'regiao' => urlencode($this->data['regiao']),
				'unidade' => urlencode($this->data['unidade']),
				'data_nascimento' => urlencode($this->data['data_nascimento']),
				'score' => $this->data['score'],
				'token' => TOKEN_LEAD,
		);
			
		$rs = Utils::enviarRequisicaoGetPostHttp(URL_ENDPOINT, $params);
		
		if($rs->success){
			$this->messages['leads'] = array('success' => true,'message' => 'Dados enviados om sucesso!');
		}else{
			$this->messages['leads'] = array('success' => false,'message' => $rs->message);
		}
	}
}