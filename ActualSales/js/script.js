/*
* Script responsável pelas máscaras de elementos dos forms
*/
$(document).ready(function(){
  
	//Máscaras
    $("input[name='data_nascimento']").mask("99/99/9999");
    $("input[name='telefone']").mask('(99) 9999-9999?9');
    
    //Popula o elemento select com base na unidade
	$("select[name='regiao']").change(function(){
		var arrUnidades;
		
		switch ($(this).val()){
			case 'Sul': 
				arrUnidades = ["Porto Alegre", "Curitiba"];
				break;
			case 'Sudeste':
				arrUnidades = ["São Paulo", "Rio de Janeiro", "Belo Horizonte"];
				break;
			case 'Centro-Oeste':
				arrUnidades = ["Brasília"];
				break;
			case 'Norte':
				arrUnidades = [""];
				break;
			case 'Nordeste':
				arrUnidades = ["Salvador", "Recife"];
				break;
		}
		
		loadUnidades(arrUnidades);
	});
	
	/**
	 * Carrega os options da Unidade pela região
	 */
	function loadUnidades(arrUnidades){
		$("select[name='unidade']").html();
		$("select[name='unidade']").find('option').remove().append('<option value="">Selecione a unidade mais próxima</option>');
		
		$.each( arrUnidades, function( key, value ) {
			var label = value;
			if(label == ''){
				var label = 'Não possui disponibilidade';
				value = 'INDISPONÍVEL';
			}
			$("select[name='unidade']").append('<option value="'+value+'">'+label+'</option>');
		});
	}
});