/*
* Script responsável pelas validações dos campos de 
* dos formuláros da página
*/
$(document).ready(function(){
	
	if($('#status').attr('data-status') == 1){
		$('#step_1').hide();
		$('#step_2').hide();
		$('#step_sucesso').show();
	}
   
	//Ação de submit do primeiro formulário
	$('#btn_step_1').click(function(){
    	$('#step_1').submit();
    });
    
	//Ação de submit do segundo formulário
    $('#btn_step_2').click(function(){
    	$('#step_2').submit();
    });
    
	//Validação do primeiro Form
	$('#step_1').formValidation({
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        trigger: 'blur',
        fields: {
        	nome : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					},
                    regexp: {
                        regexp: /^([a-záâãéêíóôõúç]+[ ]{1,1}[a-záâãéêíóôõúç]+)([a-záâãéêíóôõúç ]{0,})$/i,
                        message: 'Nome e Sobrenome'
                    }
				}
			},
        	data_nascimento : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					}
				}
			},
        	email : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					},
					emailAddress: {
                        message: 'insira um email válido'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'O valor não é um email válido'
                    }
				}
			},
        	telefone : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					},
				}
			},
        },
        onSuccess: function(e) {
        	$('#step_1').hide();
        	$('#step_2').show();
        	
        }
    });
	
	//Validação do segundo Form
	$('#step_2').formValidation({
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        trigger: 'change',
        fields: {
        	regiao : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					},
				}
			},
        	unidade : {
				validators : {
					notEmpty : {
						message : 'Campo Obrigatório'
					},
				}
			},
        },
    }).on('success.field.fv', '[name="regiao"]', function() {
    	$("select[name='unidade']").prop('disabled', false);
    }).on('err.field.fv', '[name="regiao"]', function() {
    	$("select[name='unidade']").prop('disabled', true);
    }).on('success.form.fv', function(e) {
    	
    	//append dos campos do primeiro form para o segundo
		$('#step_1 :input').not(':button').clone().hide().appendTo('#step_2');
    	
        e.preventDefault();

        var $form = $(e.target),
            fv    = $(e.target).data('formValidation');
        
        fv.defaultSubmit();
        
    });
});